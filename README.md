# RV Specials
A presentation of the specials I ordered for my Newmar VTDP.

## How'd you do that?
The presentation is based on a framework called [RevealJS](https://revealjs.com/#/). If you are familiar with basic HTML, you can use it to build presentations that can be viewed in a browser for free. If you have some familiarity with CSS and JavaScript, you can do all sorts of cool stuff.

## How can I do this too?

### Option 1: Fork this or RevealJS
If you don't know what that means, use a graphical builder as indicated below.

### Option2: Use a graphical builder
[Slides.com](https://slides.com) is a graphical builder for the same presentation framework. There is no free version for this.

If you have a recent version of Microsoft Office, you probably have access to [Office365](https://www.office.com). If not, you can sign up for free. With Office365, you can use the entire Microsoft Office Suite from right inside your web browser, including Powerpoint which is by far the easiest way to build a presentation.

Once you've built your presentation, in Powerpoint Online you'll save it to your OneDrive, a Microsoft managed online storage location, which is private just for you, but has limited storage unless you pay up.

Once you've saved your presentation, go to your OneDrive location by clicking the grid of squares in the top-right corner, then select One Drive. Locate your presentation and select it. Then click the Share button.

If you want folks to be able to edit your presentation, leave it as is and hit copy link. Anyone you send the link to will be able to edit.

If you only want people to be able to view your presentation, change the permissions by clicking 'Anyone with the link can edit' and then uncheck the 'Allow editing' box and then click Apply. Notice that the permission has now changed to 'Anyone with the link can view'.

Click the copy link button and the link will be copied to your clipboard on your computer. You can then paste it anywhere you want, including on a forum and people will be able to click the link and view your presentation in their browser.
